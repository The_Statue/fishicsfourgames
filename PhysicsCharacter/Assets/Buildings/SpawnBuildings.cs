using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBuildings : MonoBehaviour
{
    public GameObject[] BuildingPrefabs;
    //public int[,] map;
    int width = 3;
    int length = 3;
    float gap = 15;
    // Start is called before the first frame update
    void Start()
    {
        //map = new int[width, length];

        if (BuildingPrefabs.Length > 0)//&& map.Length > 0)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                    Instantiate(BuildingPrefabs[Random.Range(0, BuildingPrefabs.Length)], new Vector3((i - (width / 2.0f)) * gap, 0.75f, (j - (width / 2.0f)) * gap) + gameObject.transform.position, Quaternion.identity);
                //map[i, j] = Random.Range(0, BuildingPrefabs.Length);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
