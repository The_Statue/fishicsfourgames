using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SleepRigid : MonoBehaviour
{
    //Rigidbody body;
    // Start is called before the first frame update
    const float margin = 0.7f;
    public bool flip = false;
    Color color;
    void Start()
    {
        //GetComponent<Rigidbody>().Sleep();
        GetComponent<Rigidbody>().WakeUp();
    }

    // Update is called once per frame
    void Update()
    {

        //GetComponent<Rigidbody>().WakeUp();
    }
    void OnDrawGizmosSelected()
    {
        var joints = gameObject.GetComponents<Joint>();
        //color = new Color(0.7f, 0.4f, 0.4f);
        //color = Color.clear;
        if (color == Color.clear)
        {
            Vector3 Pos = transform.position;
            color = new Color((Pos.x % margin) + (1 - margin), (Pos.y % margin) + (1 - margin), (Pos.z % margin) + (1 - margin));
        }
        //Debug.Log(color);
        Handles.color = color;// * 0.7f;//new Color(0.1f + i * 0.3f, 0.1f + i * 0.3f, 0.1f + i * 0.3f);
        Gizmos.color = color;

        List<Vector3> verts = new List<Vector3>();
        int i = 0;
        foreach (var item in joints)
        {
            if (item.connectedBody)
            {
                var connectedPos = item.connectedBody.gameObject.transform.position;
                Handles.color = new Color(0.1f + i * 0.3f, 0.1f + i * 0.3f, 0.1f + i * 0.3f);
                i++;
                Handles.DrawLine(transform.position, connectedPos, 3);

                //Gizmos.DrawCube(connectedPos, new Vector3(0.1f, 0.1f, 0.1f));

                verts.Add(connectedPos);
            }
        }
        Mesh mesh = new Mesh();
        switch (verts.Count)
        {
            case 2:
                {
                    Vector3[] vertices = new Vector3[3]
                    {
                        transform.position,
                        verts[0],
                        verts[1],
                    };
                    mesh.vertices = vertices;

                    int[] tris = new int[3]
                    {
                        0, 2, 1
                    };
                    if (flip)
                        tris = new int[3]
                     {
                        0, 1, 2
                     };
                    mesh.triangles = tris;


                    Vector3[] normals = new Vector3[3]
                    {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
                    };
                    mesh.normals = normals;

                    Vector2[] uv = new Vector2[3]
                    {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
                    };
                    mesh.uv = uv;
                    break;
                }
            case 3:
                {
                    Vector3[] vertices = new Vector3[4]
                    {
                        transform.position,
                        verts[0],
                        verts[1],
                        verts[2],
                    };
                    mesh.vertices = vertices;

                    int[] tris = new int[6]
                    {
                        // lower left triangle
                        0, 2, 3,

                        0, 3, 1
                    };
                    if (flip)
                        tris = new int[6]
                     {
                        0, 3, 2,

                        0, 1, 3
                     };
                    mesh.triangles = tris;


                    Vector3[] normals = new Vector3[4]
                    {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
                    };
                    mesh.normals = normals;

                    Vector2[] uv = new Vector2[4]
                    {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(1, 1),
                    };
                    mesh.uv = uv;
                    break;
                }
            case 4:
                {
                    Vector3[] vertices = new Vector3[5]
                    {
                        transform.position,
                        verts[0],
                        verts[1],
                        verts[2],
                        verts[3]
                    };
                    mesh.vertices = vertices;

                    int[] tris = new int[9]
                    {
                        0, 2, 1,

                        0, 1, 4,
                        0, 4, 3
                    };
                    if (flip)
                        tris = new int[9]
                     {
                        0, 1, 2,

                        0, 4, 1,
                        0, 3, 4
                     };
                    mesh.triangles = tris;


                    Vector3[] normals = new Vector3[5]
                    {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
                    };
                    mesh.normals = normals;

                    Vector2[] uv = new Vector2[5]
                    {
            new Vector2(0, 0),
            new Vector2(1, 0),
            new Vector2(0, 1),
            new Vector2(1, 1),
            new Vector2(1, 1),
                    };
                    mesh.uv = uv;
                    break;
                }
            case 5:
                {
                    Vector3[] vertices = new Vector3[6]
                    {
                        transform.position,
                        verts[0],
                        verts[1],
                        verts[2],
                        verts[3],
                        verts[4]
                    };
                    mesh.vertices = vertices;

                    int[] tris = new int[12]
                    {
                        0, 2, 3,

                        0, 3, 1,

                        0, 4, 5,
                        0, 5, 1
                    };
                    if (flip)
                        tris = new int[12]
                     {
                        0, 3, 2,

                        0, 1, 3,

                        0, 5, 4,
                        0, 1, 5
                     };
                    mesh.triangles = tris;


                    Vector3[] normals = new Vector3[6]
                    {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
                    };
                    mesh.normals = normals;

                    Vector2[] uv = new Vector2[6]
                    {
                        new Vector2(0, 0),
                        new Vector2(1, 0),
                        new Vector2(0, 1),
                        new Vector2(1, 1),
                        new Vector2(1, 1),
                        new Vector2(1, 1)
                    };
                    mesh.uv = uv;
                    break;
                }
        }
        if (verts.Count > 1 && mesh.vertexCount > 1)
        {
            Gizmos.DrawMesh(mesh, -1, Vector3.zero, Quaternion.identity, Vector3.one * 1.0f);
            Gizmos.DrawMesh(mesh, -1, Vector3.zero, Quaternion.identity, Vector3.one * 1.0f);
        }
    }
}
