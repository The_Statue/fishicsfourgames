using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignMat : MonoBehaviour
{
    public Material[] materials;
    MeshRenderer mesh;
    // Start is called before the first frame update
    void Start()
    {
        if (materials != null)
            GetComponent<MeshRenderer>().material = materials[Random.Range(0, materials.Length)];
    }

    // Update is called once per frame
    void Update()
    {

    }
}
