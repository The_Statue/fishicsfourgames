using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamField : MonoBehaviour
{
    public int[] states = { 0, 0, 0, 0, 0 };
    public GameObject[] effects;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < states.Length; i++)
        {
            if (effects.Length > i && effects[i] != null)
            {
                effects[i].SetActive(states[i] > 0);
            }
        }
    }
}
