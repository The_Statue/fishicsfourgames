using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Ragdoll : MonoBehaviour
{
    public int count = 0;
    public float timer = 0;
    float fullTimer = 1;

    private Animator animator = null;
    public List<Rigidbody> rigidbodies = new List<Rigidbody>();

    public CharacterController cc = null;

    public bool RagdollOn
    {
        get { return !animator.enabled; }
        set
        {
            cc.enabled = !value;
            animator.enabled = !value;
            foreach (Rigidbody r in rigidbodies)
            {
                r.isKinematic = !value;
            }
        }
    }

    // Use this for initialization 
    void Start()
    {
        animator = GetComponent<Animator>();

        cc = GetComponentInParent<CharacterController>();

        rigidbodies = new List<Rigidbody>(gameObject.GetComponentsInChildren<Rigidbody>());
        foreach (Rigidbody r in rigidbodies)
        {
            r.isKinematic = true;
            r.mass *= 0.5f; //make ragdoll half mass for easier effect by fields

        }
    }

    // Update is called once per frame 
    void Update()
    {
        if (timer <= 0)
        {
            if (!RagdollOn)
                RagdollOn = (count > 0) || Input.GetKeyDown(KeyCode.Q);
            else
            {
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    cc.gameObject.transform.position = rigidbodies[0].transform.position + new Vector3(0,1,0);
                    RagdollOn = false;
                    timer = fullTimer;
                }
            }
        }
        else
        {
            timer -= Time.deltaTime;
            //RagdollOn = false;
        }
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    RagdollOn = !RagdollOn;
        //    timer = fullTimer;
        //}
    }

}