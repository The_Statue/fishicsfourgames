using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;
    public float zoomSpeed;
    public Transform target;
    public float heightOffset = 1.5f;
    public float distance;
    public float currentDistance;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // right drag rotates the camera 
        if (Input.GetMouseButton(1))
        {
            Vector3 angles = transform.eulerAngles;
            float dx = Input.GetAxis("Mouse Y");
            float dy = Input.GetAxis("Mouse X");
            if (true)
                dx = -dx;
            // look up and down by rotating around X-axis 
            angles.x = Mathf.Clamp(angles.x + dx * speed * Time.deltaTime, 0, 70);
            // spin the camera round  
            angles.y += dy * speed * Time.deltaTime;
            transform.eulerAngles = angles;
        }

        RaycastHit hit;
        if (Physics.Raycast(GetTargetPosition(), -transform.forward, out hit, distance, ~(1 << 6), QueryTriggerInteraction.Ignore) && currentDistance >= hit.distance)
        {
            // snap the camera right in to where the collision happened 
                currentDistance = hit.distance;
        }
        else
        {
            // relax the camera back to the desired distance 
            currentDistance = Mathf.MoveTowards(currentDistance, distance, Time.deltaTime * 10);
        }
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, 2, 10);
        // look at the target point 
        transform.position = GetTargetPosition() - currentDistance * transform.forward;
    }
    Vector3 GetTargetPosition()
    {
        return target.position + heightOffset * Vector3.up;
    }

}
