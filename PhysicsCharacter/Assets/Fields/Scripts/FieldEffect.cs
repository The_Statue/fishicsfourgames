using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldEffect : MonoBehaviour
{
    float timer = 0;
    float timeLimit = 5; //Time at peak size

    [Header("Field full size")]
    [Tooltip("Size of the field once it reaches full size.")]
    public Vector3 scale = new Vector3(1, 1, 1);

    // Start is called before the first frame update
    void Start()
    {
        //Convert Layer Name to Layer Number
        int LayerIndex1 = LayerMask.NameToLayer("Ground");
        int LayerIndex2 = LayerMask.NameToLayer("Building");

        //Calculate layermask to Raycast to.
        int layerMask = (1 << LayerIndex1) | (1 << LayerIndex2);


        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, layerMask, QueryTriggerInteraction.Ignore))
        {
            Vector3 pos = transform.position;
            pos.y = hit.transform.position.y;
            transform.position = pos;
        }
        //scale = transform.localScale;
        transform.localScale = new Vector3(0, 0, 0);
    }
    public float GetTotalTime()
    {
        return timeLimit;
    }

    public float GetCurrentTime()
    {
        return timer;
    }

    public float GetTimePercentage()
    {
        return GetCurrentTime() / GetTotalTime();
    }

    public float GetVolume()
    {
        return 4f / 3f * Mathf.PI * (scale.x * 1 / 2) * (scale.y * 1 / 2) * (scale.z * 1 / 2);
    }

    public void Collapse()
    {
        timer = timeLimit;
        scale = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 velocity = new Vector3();
        if (transform.localScale.x > scale.x)
            velocity.x -= 1 * Time.deltaTime;
        else
            velocity.x += 1 * Time.deltaTime;

        if (transform.localScale.y > scale.y)
            velocity.y -= 1 * Time.deltaTime;
        else
            velocity.y += 1 * Time.deltaTime;

        if (transform.localScale.z > scale.z)
            velocity.z -= 1 * Time.deltaTime;
        else
            velocity.z += 1 * Time.deltaTime;

        if ((transform.localScale.x < 0 && transform.localScale.y < 0 && transform.localScale.z < 0))
            Destroy(gameObject);

        if ((scale - (transform.localScale)).magnitude > 0.2)
            transform.localScale += velocity;
        else
        {
            if (scale == new Vector3(0, 0, 0))
                Destroy(gameObject);
            timer += Time.deltaTime;
        }

        if (timer > timeLimit)
            scale = new Vector3(0, 0, 0);
    }
}
