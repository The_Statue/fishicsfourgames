using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnField : MonoBehaviour
{
    public GameObject[] Fields;
    public int[] FieldsCount = { 0, 0, 0, 0, 0 };
    public int selected = 0;
    // Start is called before the first frame update
    void Start()
    {
        //Take one to ignore the default state
        FieldsCount[(int)States.NoGrav - 1] = 1;
        FieldsCount[(int)States.Null - 1] = 2;
        FieldsCount[(int)States.Pull - 1] = 3;
        FieldsCount[(int)States.Push - 1] = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, ~(1 << 8), QueryTriggerInteraction.Ignore))
            {
                if (FieldsCount[selected] > 0 && selected >= 0 && selected < Fields.Length && Fields[selected] != null)
                {
                    FieldsCount[selected]--;
                    GameObject field = Instantiate(Fields[selected], hit.point + new Vector3(0, 1, 0), Quaternion.identity);
                    field.GetComponent<FieldEffect>().scale = new Vector3(3, 3, 3);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            for (int i = 0; i < FieldsCount.Length; i++) //loop through to skip over empty fields
            {
                selected++;
                if (selected >= Fields.Length)
                    selected = 0;

                if (FieldsCount[selected] > 0)
                    break;
            }
        }
    }
}
