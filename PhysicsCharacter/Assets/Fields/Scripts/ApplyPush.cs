using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyPush : MonoBehaviour
{
    SimpleField parentScript;
    // Start is called before the first frame update
    void Start()
    {
        parentScript = GetComponent<SimpleField>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (PhysicsObject obj in parentScript.objects)
        {
            if (obj != null && obj.states[(int)States.Push] > 0 && !obj._rigidbody.isKinematic)
            {
                obj._rigidbody.AddForce((obj.transform.position - transform.position).normalized * 70);
            }
        }
    }
}
