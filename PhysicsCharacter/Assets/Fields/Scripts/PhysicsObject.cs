using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum States
{
    Default,
    NoGrav,
    Null,
    Pull,
    Push
}
[RequireComponent(typeof(Rigidbody))]

public class PhysicsObject : MonoBehaviour
{
    public int[] states;
    public Material[] stateMaterials;
    public Rigidbody _rigidbody = null;
    MeshRenderer mesh = null;

    // Use this for initialization 
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        mesh = GetComponent<MeshRenderer>();
        if (mesh != null && mesh.material != null && stateMaterials.Length > 0)
            stateMaterials[0] = mesh.material;

        states = new int[] { 0, 0, 0, 0, 0, 0 };
    }

    // Update is called once per frame 
    void Update()
    {
    }

    public void changeState(int state, int value)
    {
        //Debug.Log((States)state + " " + value);
        states[state] += value;

        //Debug.Log("State " + (States)state);
        if (state == (int)States.Default)
        {
            for (int i = 1; i < states.Length; i++)
                states[i] = 0;
        }

        //set material to newest state change           TODO: lerp materials together or use cool shader
        if (state >= 0 && state < stateMaterials.Length && stateMaterials[state] != null && mesh != null)
        {
            if (states[state] > 0)
                mesh.material = stateMaterials[state];
            else
                mesh.material = stateMaterials[(int)States.Default];
        }

        //ZeroGravity state
        if (state == (int)States.NoGrav && _rigidbody != null)
        {
            if (states[(int)States.NoGrav] > 0)
            {
                _rigidbody.useGravity = false;
            }
            else
            {
                _rigidbody.useGravity = true;
            }
        }
        //Null state
        if (state == (int)States.Null && _rigidbody != null)
        {
            if (states[(int)States.Null] > 0)
            {
                _rigidbody.isKinematic = true;
                FixedJoint[] joints = GetComponents<FixedJoint>();
                foreach (FixedJoint joint in joints)
                    joint.breakForce = 10;
            }
            else
            {
                _rigidbody.isKinematic = false;
            }
        }
        //Pull state
        //All handled inside of applypull
    }

    private void FixedUpdate()
    {
    }
}