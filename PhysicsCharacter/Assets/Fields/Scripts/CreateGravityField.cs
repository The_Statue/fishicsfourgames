using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGravityField : MonoBehaviour
{
    public float interval;
    float timer;
    public GameObject[] fields;
    public Vector3 size = new Vector3(1, 1, 1);
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > interval)
        {
            int random = Random.Range(0, fields.Length);
            GameObject field = Instantiate(fields[random], new Vector3(Random.Range(-5f, 5f), fields[random].transform.localScale.y / 2, Random.Range(-5f, 5f)) + gameObject.transform.position, Quaternion.identity);
            timer = 0;
            field.GetComponent<FieldEffect>().scale = size;
        }
    }
}
