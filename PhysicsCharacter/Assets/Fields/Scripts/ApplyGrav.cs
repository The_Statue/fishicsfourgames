using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyGrav : MonoBehaviour
{
    SimpleField parentScript;
    // Start is called before the first frame update
    void Start()
    {
        parentScript = GetComponent<SimpleField>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (PhysicsObject obj in parentScript.objects)
        {
            if (obj != null && obj.states[(int)States.NoGrav] > 0 && !obj._rigidbody.isKinematic)
            {
                obj._rigidbody.AddForce(new Vector3(0,10,0));
            }
        }
    }
}
