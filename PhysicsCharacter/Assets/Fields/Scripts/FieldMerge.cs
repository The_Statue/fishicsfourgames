using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldMerge : MonoBehaviour
{
    public bool merge = false;
    public States state;
    // Start is called before the first frame update
    void Start()
    {
        state = gameObject.GetComponent<SimpleField>().state;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Merge(GameObject other)
    {
        //cancel merge if already merged
        if (merge)
            return;

        FieldMerge merger = other.GetComponent<FieldMerge>(); //cancel if other has already been merged or is not correct type
        if (state != merger.state || merger.merge)
            return;

        FieldEffect effect = other.GetComponent<FieldEffect>();
        FieldEffect ourEffect = gameObject.GetComponent<FieldEffect>();

        float V = ourEffect.GetVolume() + effect.GetVolume();
        Vector3 size = Vector3.one * Mathf.Pow((3f * V) / (4f * Mathf.PI), (1f / 3f));
        //Debug.Log("Field combining: " + V + ", " + size + ", " + size * 2);
        size *= 2; //Get diameter instead of radius;



        if (effect.GetTimePercentage() > ourEffect.GetTimePercentage())
        {
            //Consider other for deletion due to closer to death
            merger.merge = true;

            ourEffect.scale = size;
            effect.Collapse();
        }
        else
        {
            //consider self for deletion
            merge = true;

            effect.scale = size;
            ourEffect.Collapse();
        };
    }
}
