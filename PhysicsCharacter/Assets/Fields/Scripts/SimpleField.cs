using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleField : MonoBehaviour
{
    public PhysicsObject[] objects = new PhysicsObject[100];
    public States state = States.Default;

    private void OnDestroy()
    {
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i] != null)
            {
                removeObject(objects[i].gameObject);
            }
        }
    }

    void addObject(GameObject gameObject)
    {
        PhysicsObject body = gameObject.GetComponent<PhysicsObject>();
        if (body)
        {
            body.changeState((int)state, 1);

            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] == null || objects[i] == body)
                {
                    objects[i] = body;
                    break;
                }
            }
        }
        CamField cam = gameObject.GetComponent<CamField>();
        if (cam)
            cam.states[(int)state - 1]++;

        Ragdoll rag = gameObject.GetComponentInParent<Ragdoll>();
        if (rag)
            rag.count++;
    }
    void removeObject(GameObject gameObject)
    {
        PhysicsObject body = gameObject.GetComponent<PhysicsObject>();
        if (body)
        {
            for (int i = 0; i < objects.Length; i++)
            {
                if (objects[i] != null && objects[i].gameObject == gameObject)
                {
                    objects[i].changeState((int)state, -1);
                    objects[i] = null;
                }
            }
        }
        CamField cam = gameObject.GetComponent<CamField>();
        if (cam)
            cam.states[(int)state - 1]--;

        Ragdoll rag = gameObject.GetComponentInParent<Ragdoll>(); ;
        if (rag)
            rag.count--;
    }

    private void OnTriggerStay(Collider other)
    {
        FieldMerge field = other.GetComponent<FieldMerge>();
        if (field)
            field.Merge(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        addObject(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        removeObject(other.gameObject);
    }
}
