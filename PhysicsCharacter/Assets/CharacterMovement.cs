using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class CharacterMovement : MonoBehaviour
{
    public float speed = 10;
    bool isGrounded = false;
    CharacterController cc;
    Animator anim;
    Vector2 moveInput = new Vector2();

    public Vector3 hitDirection;
    public float jumpVelocity = 10;
    Vector3 velocity = new Vector3();
    bool jumpInput = false;
    Transform cam;

    void Update()
    {
        moveInput.x = Input.GetAxis("Horizontal");
        moveInput.y = Input.GetAxis("Vertical");
        jumpInput = Input.GetButton("Jump");

        anim.SetFloat("Forwards", moveInput.y);
        anim.SetBool("Jump", !isGrounded);

        if (moveInput.y >= 0)
            anim.SetFloat("MoveMultiplier", 1);
        else
            anim.SetFloat("MoveMultiplier", -1);
    }

    // Start is called before the first frame update 
    void Start()
    {
        cam = Camera.main.transform;
        cc = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

    void FixedUpdate()
    {
        if (!anim.enabled)
            return;

        Vector3 delta;

        // player movement using WASD or arrow keys 

        // find the horizontal unit vector facing forward from the camera  
        Vector3 camForward = cam.forward;
        camForward.y = 0;
        camForward.Normalize();

        transform.forward = camForward;

        // use our camera's right vector, which is always horizontal 
        Vector3 camRight = cam.right;

        delta = (moveInput.x * camRight + moveInput.y * camForward) * speed;
        if (isGrounded || moveInput.x != 0 || moveInput.y != 0)
        {
            velocity.x = delta.x;
            velocity.z = delta.z;
        }

        // check for jumping 
        if (jumpInput && isGrounded)
            velocity.y = jumpVelocity;

        // check if we've hit ground from falling. If so, remove our velocity 
        if (isGrounded && velocity.y < 0)
            velocity.y = 0;

        // apply gravity after zeroing velocity so we register as grounded still 
        velocity += Physics.gravity * Time.fixedDeltaTime;

        if (!isGrounded)
            hitDirection = Vector3.zero;

        // slide objects off surfaces they're hanging on to 
        if (moveInput.x == 0 && moveInput.y == 0)
        {
            Vector3 horizontalHitDirection = hitDirection;
            horizontalHitDirection.y = 0;
            float displacement = horizontalHitDirection.magnitude;
            if (displacement > 0.1f)
                velocity -= 0.2f * horizontalHitDirection / displacement;
        }

        cc.Move(velocity * Time.fixedDeltaTime);
        isGrounded = cc.isGrounded;
    }
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hitDirection = hit.point - transform.position;
    }
}
