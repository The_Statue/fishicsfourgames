using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldCounter : MonoBehaviour
{
    public SpawnField parentScript;
    public States counterTarget;
    public Color selectedColor;
    public Color unselectedColor;

    Text text;
    Image panel;
    string baseText;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponentInChildren<Text>();
        baseText = text.text;

        panel = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = baseText + parentScript.FieldsCount[(int)counterTarget - 1];
        if (parentScript.selected == (int)counterTarget - 1)
            panel.color = selectedColor;
        else
            panel.color = unselectedColor;
    }
}
